import GoogleCloud from "@google-cloud/storage"
import Fs from "fs"
import { convert } from "./conversion.mjs"

const log = (x, formatter = x => x) => (console.log(formatter(x)), x)

const image_content_types = ["image/jpeg", "image/png", "image/bmp"]

const get_storage = ({ project_id, client_email, private_key }) =>
	new GoogleCloud.Storage({
		projectId: project_id,
		credentials: {
			client_email,
			private_key,
		},
	})

const main = async ({ secret, bucketName }) => {
	const bucket = get_storage(secret).bucket(bucketName)

	const files = (await bucket.getFiles())[0].slice(0, 20)

	const files_with_metadata = await Promise.all(
		files.map(async file => ({
			file,
			metadata: (await file.getMetadata())[0],
		})),
	)

	const images = files_with_metadata.filter(({ metadata }) =>
		image_content_types.includes(metadata.contentType),
	)

	for (const { file, metadata } of images) {
		console.log(`Converting: `, file.name)

		await convert(
			`https://storage.googleapis.com/${bucketName}/${file.name}`,
			file.name,
			async (file_name, buffer, { contentType }) => {
				await bucket.file(file_name).save(buffer, {
					contentType,
					public: true,
					metadata: {
						contentDisposition: `inline; filename="${file_name}"`,
					},
				})
			},
		)
	}
}

main({
	secret: JSON.parse(Fs.readFileSync("secret.json")),
	bucketName: "labrulez-bucket-strapi-h3hsga3",
})
